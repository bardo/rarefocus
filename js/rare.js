function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

Array.prototype.shuffle = function() {
    return this.map(function(n){ return [Math.random(), n] })
        .sort().map(function(n){ return n[1] });
}

window.reapp = {
    app: "Rarefocus 0.5",

    profile: {
        alarm: "@random1",
        track: "@random",
        theme: "@new",
        tasks: [],
        duration: 30,
        interval: 5,
        break: 15,
        pomoBlock: 4,
        trackVolume: 50,
        alarmVolume: 50,
        changeTitle: true,
        focusMode: "first",
        autoMode: false,
        focusSize: 4,
        language: "en"
    },
    session: {
        track: "",
        alarm: "",
        theme: "",
        muted: false,
        paused: false,
        done: [],
        log: [],
        focus: "",
        focusBtn: "",
        mode: "pomodoro"
    },
    system: {
        toast: false,
        timeout: false,
        timer: false,
        actual: 0,
        fakeMode: false,
        pomoCounter: 0
    },
    languages: ["en", "pt-br"],
    specials: {
        "@random1": {
            "name": "Random per session",
            "pt-br": "Aleatório por sessão"
        },
        "@random": {
            "name": "Always random",
            "pt-br": "Sempre aleatório"
        },                    
        "@new": {
            "name": "Newest",
            "pt-br": "Mais recente"
        }
    },
    messages: {
        "en": {
            "name": "English",
            "pt-br": "Inglês"
        },
        "pt-br": {
            "name": "Brazilian Portuguese",
            "pt-br": "Português Brasileiro"
        },
        // Short messages
        "About": {
            "pt-br": "Sobre"
        },
        "Add": {
            "pt-br": "Adicionar"
        },
        "Alarm": {
            "pt-br": "Alarme"
        },
        "Alarm sound": {
            "pt-br": "Som do alarme"
        },
        "Alarm volume": {
            "pt-br": "Volume do alarme"
        },
        "Author": {
            "pt-br": "Autor"
        },
        "Auto-mode": {
            "pt-br": "Modo auto"
        },
        "Background music": {
            "pt-br": "Música de fundo"
        },
        "Break": {
            "pt-br": "Pausa"
        },
        "Break duration": {
            "pt-br": "Duração da pausa"
        },
        "Close": {
            "pt-br": "Fechar"
        },
        "Credits": {
            "pt-br": "Créditos"
        },
        "Done": {
            "pt-br": "Concluído"
        },
        "Focus": {
            "pt-br": "Foco"
        },
        "Interval": {
            "pt-br": "Intervalo"
        },
        "Interval duration": {
            "pt-br": "Duração do intervalo"
        },
        "Good work!": {
            "pt-br": "Bom trabalho!"
        },
        "Language": {
            "pt-br": "Idioma"
        },
        "License": {
            "pt-br": "Licença"
        },
        "Log": {
            "pt-br": "Histórico"
        },
        "Music volume": {
            "pt-br": "Volume da música"
        },
        "Name": {
            "pt-br": "Nome"
        },
        "Noname task": {
            "pt-br": "Tarefa sem nome"
        },
        "Pause button": {
            "pt-br": "Botão pause"
        },
        "Play button": {
            "pt-br": "Botão play"
        },
        "Pomodoro": {
            "pt-br": "Pomodoro"
        },
        "Request Failed": {
            "pt-br": "Requisição falhou"
        },
        "Settings": {
            "pt-br": 'Ajustes'
        },
        "Stop button": {
            "pt-br": "Botão de parar"
        },
        "Success": {
            "pt-br": "Bem-sucedido"
        },
        "Task duration": {
            "pt-br": "Duração da tarefa"
        },
        "Tasks": {
            "pt-br": "Tarefas"
        },
        "Tasks in focus": {
            "pt-br": "Tarefas em foco"
        },
        "Theme": {
            "pt-br": "Tema"
        },
        "Timer": {
            "pt-br": "Temporizador"
        },
        "Track": {
            "pt-br": "Trilha"
        },
        "Type": {
            "pt-br": "Tipo"
        },
        // Phrases
        "All data is saved in your web browser (Local Storage).": {
            "pt-br": "Todos os dados são salvos no seu navegador (Local Storage)."
        },
        "In settings you can define how many.": {
            "pt-br": "Nos ajustes você pode definir quantas."
        },
        "Key features include background music to help you focus.": {
            "pt-br": "Funcionalidades principais incluem música de fundo para ajudar você a ter foco."
        },
        "Only some tasks are shown in timer page (focus tasks).": {
            "pt-br": "Só algumas tarefas são mostradas no temporizador por vez (tarefas em foco)."
        },
        "Pomodoros per block": {
            "pt-br": "Pomodoros por bloco"
        },
        "Rarefocus is a pomodoro timer and task manager developed by": {
            "pt-br": "Rarefocus é um temporizador pomodoro e gerenciador de tarefas desenvolvido por"
        },
        "Timer in page title": {
            "pt-br": "Temporizador no título da página"
        },
        "Visit the project page": {
            "pt-br": "Visite a página do projeto"
        },
        "You can add and remove tasks.": {
            "pt-br": "Você pode adicionar e remover tarefas."
        },
        "You can customise Rarefocus according to your needs.": {
            "pt-br": "Você pode personalizar o Rarefocus de acordo com suas necessidades."
        },
        "Your changes has been saved": {
            "pt-br": "Suas modificações foram salvas"
        }
    },

    tracks: {},
    alarms: {},
    themes: {},

    gettext: function(msg, lang = this.profile.language) {
        if (typeof msg === 'string') {
            if (msg in window.reapp.messages) {
                if (lang in window.reapp.messages[msg]) {
                    return window.reapp.messages[msg][lang]
                } else {
                    if ("name" in window.reapp.messages[msg]) {
                        return window.reapp.messages[msg]["name"]
                    }
                }
            }
        } else if (msg instanceof jQuery || msg instanceof HTMLElement) {
            var aux = $(msg).attr("value") || $(msg).text()
            if (aux in window.reapp.messages) {
                if (lang in window.reapp.messages[aux]) {
                    if ($(msg).attr("value") === undefined) {
                        $(msg).attr("value", aux)
                    }        
                    $(msg).text(window.reapp.messages[aux][lang])
                    return true
                } else if ($(msg).attr("value")) {
                    if ("name" in window.reapp.messages[$(msg).attr("value")]) {
                        $(msg).text(window.reapp.messages[$(msg).attr("value")]["name"])
                    } else {
                        $(msg).text($(msg).attr("value"))
                    }
                } else if ("name" in window.reapp.messages[aux]) {
                    $(msg).text(window.reapp.messages[aux]["name"])
                }
            }
            msg = aux
        } else {
            if (lang in msg) {
                return msg[lang]
            } else {
                if ("name" in msg) {
                    return msg.name
                }
            }
        }
        console.log("Gettext: " + msg)
        return msg
    },

    setPlay: function(file) {
        clearTimeout(window.reapp.system.timeout)
        $("#setrack")[0].src = file
        $("#setrack")[0].volume = window.reapp.profile.trackVolume / 100
        $("#setrack")[0].play()
        window.reapp.settimeout = window.setTimeout(() => {
            $("#setrack")[0].pause()
        }, 3000)
    },
    getFromList: function(lkey, list, buff = "@none") {
        var bid = lkey
        switch (lkey) {
            case "@random1":
                if (buff != "@none") {
                    if (window.reapp.session[buff] != "") {
                        bid = window.reapp.session[buff]
                        break
                    }
                }
            case "@random":
                var _k = Object.keys(list)
                bid = _k[Math.floor((Math.random() * 1000) % (_k.length-3))+3]
                if (buff != "@none") {
                    window.reapp.session[buff] = bid
                }
                break
            case "@new":
                var _k = Object.keys(list)
                bid = _k[_k.length -1]
                break
        }
        return bid
    },
    playBg: () => {
        $("#timer-play").hide()
        $("#timer-pause").show()
        $("#timer-stop").show()
        $("#prog-timer").addClass("active")
        $("#timer").addClass("active")
        $("#timer-modes input").attr("disabled", "disabled")
        $("#task-block").hide()
        $("#done-block").hide()
        if (window.reapp.session.paused) {
            $("#bgtrack")[0].play()
            window.reapp.session.paused = false
            window.reapp.refreshTimer()
            window.reapp.logIt("resume", window.reapp.gettext("Play button"))
        } else {
            var bid = window.reapp.getFromList(window.reapp.profile.track, window.reapp.tracks, "track")
            var audio = "tracks/" + window.reapp.tracks[bid].file

            $("#bgtrack")[0].src = audio
            if ((window.reapp.session.mode == "pomodoro") && (!window.reapp.session.muted)) {
                $("#bgtrack")[0].volume = window.reapp.profile.trackVolume / 100
            } else {
                $("#bgtrack")[0].volume = 0
            }
            $("#bgtrack")[0].play()
            if (window.reapp.session.focus != "") {
                var _b = $("<b>").text(window.reapp.session.focus)
                $("#focus").text(window.reapp.gettext("Focus") + ": ")
                $("#focus").append(_b)
            }
        
            if (window.reapp.system.fakeMode)
                window.reapp.system.actual = 0.1 * window.reapp.getMinutes()
            else
                window.reapp.system.actual = 60 * window.reapp.getMinutes()

                window.reapp.session.paused = false
        
            window.reapp.system.timer = setInterval(() => {
                if (!window.reapp.session.paused) {
                    window.reapp.system.actual--;
                    window.reapp.refreshTimer()
                
                    if (window.reapp.system.actual <= 0) {
                        window.reapp.stopBg("finish")
                        var aid = window.reapp.getFromList(window.reapp.profile.alarm, window.reapp.alarms, "alarm")
                        $("#altrack")[0].src = "alarms/" + window.reapp.alarms[aid].file
                        $("#altrack")[0].volume = window.reapp.profile.alarmVolume / 100
                        $("#altrack")[0].play()
                        window.reapp.logIt("finish", window.reapp.gettext("Successful"))
                    }
                }
            }, 1000);
            window.reapp.logIt("start", window.reapp.gettext("Noname task"))
        }
    },
    pauseBg: () => {
        $("#timer-play").show()
        $("#timer-pause").hide()
        $("#bgtrack")[0].pause()        
        window.reapp.session.paused = true
        $("#prog-timer").removeClass("active")
        $("#timer").removeClass("active")
        window.reapp.refreshTimer()
        window.reapp.logIt("pause", window.reapp.gettext("Pause button"))
    },
    muteBg: () => {
        window.reapp.session.muted = !window.reapp.session.muted
        if (window.reapp.session.muted) {
            $("#bgtrack")[0].volume = 0
            $("#timer-mute i").removeClass("bx-volume-full").addClass("bx-volume-mute")
        } else {
            $("#bgtrack")[0].volume = window.reapp.profile.trackVolume / 100
            $("#timer-mute i").removeClass("bx-volume-mute").addClass("bx-volume-full")
        }
    },
    stopBg: (state = "cancel") => {
        $("#app-title").text(window.reapp.app)
        $("#timer-play").show()
        $("#timer-stop").hide()
        $("#timer-pause").hide()
        $("#timer").removeClass("active")
        $("#timer-modes input").removeAttr("disabled")
        $("#bgtrack")[0].pause()
        window.reapp.session.paused = false
        clearInterval(window.reapp.system.timer)
        document.title = window.reapp.app
        $("#prog-timer").width(0)
        if (state == "finish") {
            if (window.reapp.session.focus != "") {
                $(window.reapp.session.focusBtn).appendTo("#timer-done")
                $(window.reapp.session.focusBtn).off("click").on("click", (val) => {
                    var _iok = $("<i>").addClass('bx bx-task task-done').insertAfter(val.target)

                    // window.reapp.profile.tasks.splice(0, 0, $(val.target).text())
                    window.reapp.session.done.splice(window.reapp.session.done.indexOf($(val.target).text()),1)
                    $(val.target).remove()
                    console.log()
                    window.reapp.addTask($(val.target).text())
                    $("#task-first, #task-last, #task-random").addClass("badge")
                })
                if (window.reapp.session.mode == "pomodoro") {
                    window.reapp.system.pomoCounter++
                    window.reapp.session.done.push(window.reapp.session.focus)
                    window.reapp.delTask(window.reapp.session.focus)
                }
            } else {
                switch (window.reapp.session.mode) {
                    case "pomodoro": 
                        window.reapp.system.pomoCounter++
                        window.reapp.session.done.push("@task")
                        var _iok = $("<i>").addClass('bx bx-task task-done')
                        $("#timer-done").append(_iok)
                        break
                    case "break":
                        window.reapp.session.done.push("@task")
                        var _iok = $("<i>").addClass('icon icon-emoji task-done')
                        $("#timer-done").append(_iok)
                        break
                }
            }
            if (window.reapp.profile.autoMode) {
                var _nm = ""
                switch (window.reapp.session.mode) {
                    case "pomodoro":
                        if (window.reapp.system.pomoCounter == window.reapp.profile.pomoBlock) {
                            window.reapp.system.pomoCounter = 0
                            _nm = "break"
                        } else {
                            _nm = "interval"
                        }
                        break
                    case "interval":
                    case "break":
                        _nm = "pomodoro"
                        break
                }                
                window.reapp.session.mode = _nm
            }
        } else {
            if (window.reapp.session.mode == "pomodoro") {
                window.reapp.logIt("cancel", window.reapp.gettext("Stop button"))
                var _ino = $("<i>").addClass('bx bx-x-circle task-done')
                $("#timer-done").append(_ino)
            }
        }
        window.reapp.session.focus = ""
        $("#focus").text("")       
        window.reapp.fullTimer()
        window.reapp.setTheme()
        if ((window.reapp.profile.autoMode) && (state == "finish")) {
            window.reapp.playBg()
        }
    },
    getMinutes: () => {
        switch (window.reapp.session.mode) {
            case "pomodoro":
                return window.reapp.profile.duration
            case "interval":
                return window.reapp.profile.interval
            case "break":
                return window.reapp.profile.break
            default:
                return 0
        }
    },
    fullTimer: () => {
        $("#timer").text(window.reapp.getMinutes() + ':00')
        if (window.reapp.session.mode == "pomodoro") {
            $("#task-block").show()
            $("#done-block").show()
        } else {
            $("#task-block").hide()
            $("#done-block").hide()
        }
    },
    refreshTimer: () => {
        var min = Math.floor(window.reapp.system.actual / 60)
        var sec = window.reapp.system.actual % 60
        if (sec < 10) {
            sec = '0' + sec
        }       
        $("#timer").text(min + ':' + sec);
        var perc = Math.floor( (window.reapp.system.actual * 100) / (window.reapp.getMinutes() * 60) )
        $("#prog-timer").width(perc + "%")
    
        if (window.reapp.profile.changeTitle) {
            if (window.reapp.session.paused) {
                document.title = "(" + min + ":" + sec + ")"
            } else {
                document.title = min + ":" + sec
            }
        }
    },
    playTask: (tasknum = -1) => {
        if (tasknum == -1) {
            window.reapp.session.focusBtn = false
            window.reapp.session.focus = ""
        } else {
            window.reapp.session.focusBtn = "#taskbtn" + tasknum
            window.reapp.session.focus = $(window.reapp.session.focusBtn).text()
        }
        window.reapp.playBg()
    },
    addTaskButton: (cont, tasknum) => {
        var _b = $("<button>").addClass("btn task-play").attr("id", "taskbtn" + tasknum).text(window.reapp.profile.tasks[tasknum])
        _b.on("click", () => {
            window.reapp.playTask(tasknum)
        })
        $(cont).append(_b)
    },
    refreshTasks: (mode = "none") => {        
        if (mode == "none" && ($("#timer-tasks .btn").length == 3)) {
            mode = window.reapp.profile.focusMode
        }
        if (mode != "none") {
            $("#timer-tasks .btn").remove()

            var _brand = $("<button>").addClass("btn task-fill").attr("id", "task-first")
            _brand.on("click", () => {
                window.reapp.refreshTasks("first")
            })
            var _irand = $("<i>").addClass('bx bx-sort-a-z')
            _brand.append(_irand)
            $("#timer-tasks").append(_brand)
            var _brand = $("<button>").addClass("btn task-fill").attr("id", "task-last")
            _brand.on("click", () => {
                window.reapp.refreshTasks("last")
            })
            var _irand = $("<i>").addClass('bx bx-sort-z-a')
            _brand.append(_irand)
            $("#timer-tasks").append(_brand)
            var _brand = $("<button>").addClass("btn task-fill").attr("id", "task-random")
            _brand.on("click", () => {
                window.reapp.refreshTasks("random")
            })
            var _irand = $("<i>").addClass('bx bx-shuffle')
            _brand.append(_irand)
            $("#timer-tasks").append(_brand)
        }
        switch (mode) {
            case "first":
                for (var i = 0; i < window.reapp.profile.focusSize; i++) {
                    if (window.reapp.profile.tasks.length > i)
                        window.reapp.addTaskButton("#timer-tasks", i)
                }
                break
            case "last":
                for (var i = 1; i <= window.reapp.profile.focusSize; i++) {
                    pos = window.reapp.profile.tasks.length - i
                    if (pos >= 0)
                        window.reapp.addTaskButton("#timer-tasks", pos)
                }
                break
            case "random":
                pos = Object.keys(window.reapp.profile.tasks).shuffle()
                for (var i = 0; i < window.reapp.profile.focusSize; i++) {
                    if (window.reapp.profile.tasks.length > i)
                        window.reapp.addTaskButton("#timer-tasks", pos[i])
                }
                break
        }
        window.reapp._saveTasks()
        $("#task-list").empty()
        window.reapp._justListTasks()
    },
    resetDone: () => {
        $("#timer-done .btn").remove()
        $("i.task-done").remove()
        var _btrash = $("<button>").addClass("btn task-clear").attr("id", "task-clear")
        _btrash.on("click", () => {
            window.reapp.resetDone()
        })
        var _itrash = $("<i>").addClass('bx bx-trash')
        _btrash.append(_itrash)
        $("#timer-done").append(_btrash)
    },
    loadSettings: () => {
        window.reapp.profile.changeTitle = (localStorage.changeTitle == "true")
        window.reapp.profile.autoMode = (localStorage.autoMode == "true")
        window.reapp.profile.pomoBlock = (localStorage.pomoBlock > 0) ? localStorage.pomoBlock : 4
        window.reapp.profile.focusSize = (localStorage.focusSize > 0) ? localStorage.focusSize : 4
        if (localStorage.track) {
            window.reapp.profile.track =  localStorage.track
        }
        if (localStorage.alarm) {
            window.reapp.profile.alarm =  localStorage.alarm
        }
        if (localStorage.theme) {
            window.reapp.profile.theme = localStorage.theme
        }
        if (localStorage.language) {
            window.reapp.profile.language = localStorage.language
        }
        window.reapp.profile.alarmVolume = (isNumeric(localStorage.alarmVolume)) ? localStorage.alarmVolume : 50
        window.reapp.profile.trackVolume = (isNumeric(localStorage.trackVolume)) ? localStorage.trackVolume : 50
        window.reapp.profile.duration = (isNumeric(localStorage.duration)) ? localStorage.duration : 25
        window.reapp.profile.interval = (isNumeric(localStorage.interval)) ? localStorage.interval : 5
        window.reapp.profile.break = (isNumeric(localStorage.break)) ? localStorage.break : 15
        window.reapp.settingsRefresh()
    },
    notify: (th, tt) => {
        $("#toast-header").text(th)
        $("#toast-text").text(tt)
        $("#toast").addClass("active")
        window.reapp.system.toast = setTimeout( () => {
            $("#toast").removeClass("active")
        }, 5000)
    },
    openPage: (name) => {
        $('.content').removeClass('active')
        $('#page-' + name).addClass('active')
        $('.tab-item').removeClass('active')
        $('#tab-' + name).addClass('active')
    },
    settingsRefresh: () => {     
        $("#switch-title")[0].checked = (window.reapp.profile.changeTitle == true)
        if (window.reapp.tracks[window.reapp.profile.track]) {
            $("#track-text").text(window.reapp.tracks[window.reapp.profile.track].name)
        }
        $("#volume-text").text(window.reapp.profile.trackVolume)
        $("#volume-range")[0].value = window.reapp.profile.trackVolume
        $("#alarm-volume-text").text(window.reapp.profile.alarmVolume)
        $("#alarm-volume-range")[0].value = window.reapp.profile.alarmVolume
        $("#switch-auto")[0].checked = (window.reapp.profile.autoMode == true)
        $("#pomo-block")[0].value = window.reapp.profile.pomoBlock
        $("#focus-size")[0].value = window.reapp.profile.focusSize
        $("#pomo-text").text(window.reapp.profile.duration)
        $("#pomo-range")[0].value = window.reapp.profile.duration
        $("#inter-text").text(window.reapp.profile.interval)
        $("#inter-range")[0].value = window.reapp.profile.interval
        $("#break-text").text(window.reapp.profile.break)
        $("#break-range")[0].value = window.reapp.profile.break
        $.each($("input[name=track-radio]"), (ind, val) => {
            if (window.reapp.profile.track == val.value) {
                val.checked = true
            } else {
                val.checked = false
            }            
        })
        $.each($("input[name=lang-radio]"), (ind, val) => {
            if (window.reapp.profile.language == val.value) {
                val.checked = true
            } else {
                val.checked = false
            }            
        })
        $.each($("input[name=alarm-radio]"), function (id, val) {
            if (window.reapp.profile.alarm == val.value) {
                val.checked = true
            } else {
                val.checked = false
            }
        })
        $.each($("input[name=theme-radio]"), function (id, val) {
            if (window.reapp.profile.theme == val.value) {
                val.checked = true
            } else {
                val.checked = false
            }
        })
    },
    refreshMode: () => {
        $.each($("input[name=timer-mode]"), (ind, val) => {
            if (window.reapp.session.mode == val.value) {
                val.checked = true
            } else {
                val.checked = false
            }            
        })
        window.reapp.fullTimer()
    },
    start: () => {
        $("#timer-pause").hide()
        $("#timer-stop").hide()

        window.reapp.loadSettings();

        window.reapp.listTracks()
        window.reapp.listAlarms()        
        window.reapp.listThemes()
    
        var tmod = $("input[name=timer-mode]")
        tmod[0].checked = true
        $(tmod[0]).on("click", () => {
            window.reapp.session.mode = "pomodoro"
            window.reapp.refreshMode()
        })
        $(tmod[0]).next().on("click", () => {
            window.reapp.session.mode = "pomodoro"
            window.reapp.refreshMode()
        })
        $(tmod[1]).on("click", () => {
            window.reapp.session.mode = "interval"
            window.reapp.refreshMode()
        })
        $(tmod[1]).next().on("click", () => {
            window.reapp.session.mode = "interval"
            window.reapp.refreshMode()
        })
        $(tmod[2]).on("click", () => {
            window.reapp.session.mode = "break"
            window.reapp.refreshMode()
        })
        $(tmod[2]).next().on("click", () => {
            window.reapp.session.mode = "break"
            window.reapp.refreshMode()
        })
        
        document.title = window.reapp.app
        $("#timer-play").on("click", window.reapp.playBg)
        $("#timer-stop").on("click", window.reapp.stopBg)
        $("#timer-pause").on("click", window.reapp.pauseBg)
        $("#timer-mute").on("click", window.reapp.muteBg)

        $("#toast-close").on("click", () => {
            clearInterval(window.reapp.system.toast)
            $("#toast").removeClass("active")
        })
        $("#rare-settings").on("click", window.reapp.settingsMenu)
        $("#settings-button").on("click", window.reapp.settingsApply)
        
        $('.tab-item a').click( (obj) => {
            window.reapp.openPage(obj.currentTarget.id)
            window.reapp.settingsRefresh()
        })

        $("#switch-title").on("change", function () {
            window.reapp.profile.changeTitle = this.checked
            if (window.reapp.profile.changeTitle || (window.reapp.system.actual > 0) ) {
                document.title = window.reapp.app
            }
            localStorage.setItem('changeTitle', window.reapp.profile.changeTitle);
            window.reapp.settingsRefresh()
        })
        
        $("#switch-auto").on("change", function () {
            window.reapp.profile.autoMode = this.checked
            localStorage.setItem('autoMode', window.reapp.profile.autoMode);
            window.reapp.settingsRefresh()
        })

        $("#pomo-block").on("change", function () {
            window.reapp.profile.pomoBlock = this.value
            localStorage.setItem('pomoBlock', window.reapp.profile.pomoBlock);
            window.reapp.settingsRefresh()
        })

        $("#focus-size").on("change", function () {
            window.reapp.profile.focusSize = this.value
            localStorage.setItem('focusSize', window.reapp.profile.focusSize);
            window.reapp.settingsRefresh()
        })

        $("#volume-range").on("change", () => {
            window.reapp.profile.trackVolume = $("#volume-range")[0].value
            $("#volume-text").text(window.reapp.profile.trackVolume)
            if (window.reapp.system.actual > 0) {
                $("#bgtrack")[0].volume = window.reapp.profile.trackVolume / 100
            }
            localStorage.setItem('trackVolume', window.reapp.profile.trackVolume);
            window.reapp.settingsRefresh()
        })
        $("#alarm-volume-range").on("change", () => {
            window.reapp.profile.alarmVolume = $("#alarm-volume-range")[0].value
            $("#alarm-volume-text").text(window.reapp.profile.alarmVolume)
            localStorage.setItem('alarmVolume', window.reapp.profile.alarmVolume);
            window.reapp.settingsRefresh()
        })
        $("#pomo-range").on("change", () => {
            window.reapp.profile.duration = $("#pomo-range")[0].value
            $("#pomo-text").text(window.reapp.profile.duration)
            localStorage.setItem('duration', window.reapp.profile.duration);
            window.reapp.settingsRefresh()
        })
        $("#inter-range").on("change", () => {
            window.reapp.profile.interval = $("#inter-range")[0].value
            $("#inter-text").text(window.reapp.profile.interval)
            localStorage.setItem('interval', window.reapp.profile.interval);
            window.reapp.settingsRefresh()
        })
        $("#break-range").on("change", () => {
            window.reapp.profile.break = $("#break-range")[0].value
            $("#break-text").text(window.reapp.profile.break)
            localStorage.setItem('break', window.reapp.profile.break);
            window.reapp.settingsRefresh()
        })
        window.reapp.settingsRefresh()

        // Task Manager section
        var runnow = (i) => {
            aux = $("#task-input")[0].value
            window.reapp.addTask(aux)
            $("#task-first, #task-last, #task-random").addClass("badge")
        }
        $("#task-button").click(runnow)
        $("#task-input").keypress((e) => {
            if (e.keyCode == '13')
            runnow()
        })
        
        if (localStorage.tasks) {
            window.reapp.profile.tasks = JSON.parse(localStorage.tasks)
        }
        if (! $.isArray(window.reapp.profile.tasks)) {
            window.reapp.profile.tasks = []
        }
        window.reapp.listLanguages()
        window.reapp.listTasks(window.reapp.profile.focusMode)
        window.reapp.resetDone()
        window.reapp.fullTimer()
        window.reapp.logIt("zero", window.reapp.gettext("Good work!"))
    },
    setTheme: () => {
        var tid = window.reapp.getFromList(window.reapp.profile.theme, window.reapp.themes, "theme")

        var th = window.reapp.themes[tid]

        if (th.hot)
            $(':root').css('--rare-hot', th.hot)
        else
            $(':root').css('--rare-hot', "blue")

        if (th.background)
            $(':root').css('--rare-background', th.background)
        else
            $(':root').css('--rare-background', "transparent")

        if (th.text)
            $(':root').css('--rare-text', th.text)
        else
            $(':root').css('--rare-text', "black")

        if (th.bar)
            $(':root').css('--rare-bar', th.bar)
        else
            $(':root').css('--rare-bar', "transparent")

        if (th.bgtexture)
            $(':root').css('--rare-bgtexture', th.bgtexture)
        else
            $(':root').css('--rare-bgtexture', "none")

        if (th.bartexture)
            $(':root').css('--rare-bartexture', th.bartexture)
        else
            $(':root').css('--rare-bartexture', "none")
    },
    clickOnTrack: function () {
        if (this.localName == "label")
            var el = $(this).prev()[0]
        else
            var el = $(this)[0]
        localStorage.setItem("track", el.value)
        if (el.value[0] != '@') {
            window.reapp.setPlay("tracks/" + window.reapp.tracks[el.value].file)
            window.reapp.settimeout = window.setTimeout(() => {
                $("#setrack")[0].pause()
            }, 3000)
        }
        window.reapp.loadSettings()
    },
    clickOnAlarm: function () {
        if (this.localName == "label")
            var el = $(this).prev()[0]
        else
            var el = $(this)[0]
        localStorage.setItem("alarm", el.value)
        if (el.value[0] != '@') {
            window.reapp.setPlay("alarms/" + window.reapp.alarms[el.value].file)
            window.reapp.settimeout = window.setTimeout(() => {
                $("#setrack")[0].pause()
            }, 3000)
        }
        window.reapp.loadSettings()
    },
    clickOnTheme: function () {
        if (this.localName == "label")
            var el = $(this).prev()[0]
        else
            var el = $(this)[0]
        localStorage.setItem("theme", el.value)
        window.reapp.loadSettings()
        if (el.value[0] != '@') {
            window.reapp.setTheme()
        }
    },
    clickOnLang: function() {
        if (this.localName == "label")
            var el = $(this).prev()[0]
        else
            var el = $(this)[0]
        console.log(el)
        localStorage.setItem("language", el.value)
        window.reapp.loadSettings()
        window.reapp.refreshLanguage()
    },
    refreshLanguage: function() {
        $.each($(".gettext"), function (ind, val) {
            window.reapp.gettext(val)
        })
    },
    listTracks: () => {
        $.getJSON('tracks/list.json', (data) => {
            window.reapp.tracks = $.extend({}, window.reapp.specials, data.list)
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( window.reapp.gettext("Request Failed") + ": " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.tracks, function (ind, val) {
                var odiv = $("<div>").addClass("radio")
                var optio = $("<input>", { type: "radio", 
                    name: "track-radio", value: ind}).on("click", window.reapp.clickOnTrack)
                odiv.append(optio)
                var olab = $("<label>").addClass("radio-label gettext").text( window.reapp.gettext(val) )
                olab.attr("value", window.reapp.gettext(val, "en")).on("click", window.reapp.clickOnTrack)
                odiv.append(olab)                
                $("#track-sel").append(odiv)

                /* credits */
                if (val.author) {
                    var _row = $("<tr>")
                    var _cna = $("<td>").text( window.reapp.gettext(val) )
                    var _cty = $("<td>").addClass("opt").text(window.reapp.gettext("Track"))
                    var _cau = $("<td>").text(val.author)
                    var _cli = $("<td>").addClass("opt").text(val.license)
                    _row.append(_cna).append(_cty).append(_cau).append(_cli)
                    $("#credits").append(_row)
                }
            })
        })
    },
    listLanguages: () => {
        $.each(window.reapp.languages, function (ind, val) {
            var odiv = $("<div>").addClass("radio")
            var optio = $("<input>", { type: "radio", 
                name: "lang-radio", value: val}).on("click", window.reapp.clickOnLang)
            odiv.append(optio)
            var olab = $("<label>").addClass("radio-label gettext").text( window.reapp.gettext(val) )
            olab.attr("value", val).on("click", window.reapp.clickOnLang)
            odiv.append(olab)                
            $("#lang-sel").append(odiv)
        })
        window.reapp.refreshLanguage()
    },
    listAlarms: () => {
        $.getJSON('alarms/list.json', (data) => {            
            window.reapp.alarms = $.extend({}, window.reapp.specials, data.list)
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( "Request Failed: " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.alarms, function (ind, val) {
                var odiv = $("<div>").addClass("radio")
                var optio = $("<input>", { type: "radio", 
                    name: "alarm-radio", value: ind}).on("click", window.reapp.clickOnAlarm)
                odiv.append(optio)
                var olab = $("<label>").addClass("radio-label gettext").text( window.reapp.gettext(val) )
                olab.attr("value", window.reapp.gettext(val, "en")).on("click", window.reapp.clickOnAlarm)
                odiv.append(olab)                
                $("#alarm-sel").append(odiv)

                /* credits */
                if (val.author) {
                    var _row = $("<tr>")
                    var _cna = $("<td>").text( window.reapp.gettext(val) )
                    var _cty = $("<td>").addClass("opt").text(window.reapp.gettext("Alarm"))
                    var _cau = $("<td>").text(val.author)
                    var _cli = $("<td>").addClass("opt").text(val.license)
                    _row.append(_cna).append(_cty).append(_cau).append(_cli)
                    $("#credits").append(_row)
                }
            })
        })
    },
    listThemes: () => {
        $.getJSON('js/themes.json', (data) => {
            window.reapp.themes = $.extend({}, window.reapp.specials, data.list)
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( "Request Failed: " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.themes, function (ind, val) {
                var odiv = $("<div>").addClass("radio")
                var optio = $("<input>", { type: "radio", 
                    name: "theme-radio", value: ind}).on("click", window.reapp.clickOnTheme)
                odiv.append(optio)
                var olab = $("<label>").addClass("radio-label").addClass("gettext").text( window.reapp.gettext(val) ).attr("value", window.reapp.gettext(val, "en")).on("click", window.reapp.clickOnTheme)
                odiv.append(olab)                
                $("#theme-sel").append(odiv)
            })
            window.reapp.setTheme()
        })
    },
    _justListTasks: () => {
        $.each(window.reapp.profile.tasks, function (ind, val) {
            window.reapp._justAddTask(val)
        })
    },
    listTasks: (mode = "none") => {
        window.reapp.refreshTasks(mode)
    },
    _justAddTask: (tex) => {
        var li = $("<span>").addClass("chip some-task").text(tex)
        var deli = $("<a>", { "aria-label": window.reapp.gettext("Close"), "role": "button"}).addClass("btn btn-clear").click( (i) => {
            window.reapp.addDone(tex)
            window.reapp.delTask(tex, false)
        })
        li.append(deli)
        $("#task-list").append(li)
    },
    _saveTasks: () => {
        localStorage.setItem("tasks", JSON.stringify(window.reapp.profile.tasks))
    },
    addTask: (tex) => {
        window.reapp.profile.tasks.push(tex)
        $("#task-input")[0].value = ""
        window.reapp._saveTasks()
        window.reapp.refreshTasks()
    },
    delTask: (tex, auto = true) => {
        pos = window.reapp.profile.tasks.indexOf(tex)
        if (pos > -1 ) {
            window.reapp.profile.tasks = window.reapp.profile.tasks.slice(0,  pos).concat(window.reapp.profile.tasks.slice(pos+1))
        }
        $.each($("#timer-tasks .btn"), (ind, val) => {
            if (tex == $(val).text()) {
                $(val).remove()
            }            
        })
        pos = window.reapp.profile.tasks.indexOf(tex)
        window.reapp._saveTasks()
        window.reapp.refreshTasks()
        if (!auto)
            $("#task-first, #task-last, #task-random").addClass("badge")
    },
    addDone: (tex) => {
        window.reapp.session.done.push(tex)
        var li = $("<li>").addClass("done-task").text(tex)
        $("#done-list").append(li)
    },
    logIt: (operation, description) => {
        var tnow = new Date().toLocaleString()
        window.reapp.session.log.push(operation, tnow, description)
        if ((operation == "zero") ||
                ((operation == "start") && 
                (['pomodoro', 'break'].indexOf(window.reapp.session.mode) >= 0))) {
            var lnew = $("<div>").addClass("timeline-item")
            var llef = $("<div>").addClass("timeline-left")
            var lico = $("<a>").addClass("timeline-icon")
            var tdes = (window.reapp.session.focus != "") ? window.reapp.session.focus : description
            if (operation == "start") {
                lico.addClass("icon-lg")
                if (window.reapp.session.mode == 'pomodoro')
                    var lsta = $("<i>").addClass("rare-icon icon icon-time")
                else {
                    var lsta = $("<i>").addClass("rare-icon icon icon-photo")
                    tdes = "Break time!"
                }
                lico.append(lsta)    
            }
            llef.append(lico)
            lnew.append(llef)
            var ltlc = $("<div>").addClass("timeline-content")
            var lttl = $("<div>").addClass("tile-content")
            var lsub = $("<div>").addClass("tile-subtitle").text(tdes + " (" + tnow + ")")
            lttl.append(lsub)
            ltlc.append(lttl)
            lnew.append(ltlc)
            $("#log-tl").append(lnew)
        } else {
                var ltil = $("<div>").addClass("tile-title").text(description + " - " + window.reapp.session.mode + " (" + tnow + "): ")
                $(".tile-content:last").append(ltil)
                switch (operation) {
                    case "cancel":
                        if (window.reapp.session.mode == 'pomodoro')
                            $(".rare-icon:last").removeClass("icon-time").addClass("icon-cross")
                        else
                            $(".rare-icon:last").removeClass("icon-photo").addClass("icon-refresh")
                        break
                    case "finish": 
                        if (window.reapp.session.mode == 'pomodoro')
                            $(".rare-icon:last").removeClass("icon-time").addClass("icon-check")
                        else
                            $(".rare-icon:last").removeClass("icon-photo").addClass("icon-emoji")
                        break
                }
        }
    }
}

