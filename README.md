# Rarefocus

A pomodoro timer for web with some unique features. Available in https://bardo.codeberg.page/rarefocus

## Screenshots

| Timer | Settings | Tasks |
|:-:|:-:|:-:|
| ![Timer](img/desk1.png) | ![Settings](img/desk2.png) | ![Tasks](img/desk3.png) |
| ![Timer](img/mobi1.png) | ![Settings](img/mobi2.png) | ![Tasks](img/mobi3.png) |

| Timer | Log | About |
|:-:|:-:|:-:|
| ![Timer](img/desk4.png) | ![Log](img/desk5.png) | ![About](img/desk6.png) |
| ![Timer](img/mobi4.png) | ![Log](img/mobi5.png) | ![About](img/mobi6.png) |

## Features

* ✔️ Simple timer
* ✔️ Configure pomodoro duration
* ✔️ Choose alarm sound (9 alarms)
* ✔️ Choose music to play (29 tracks)
* ✔️ Random music
* ✔️ Save in LocalStorage
* ✔️ Theme support (9 themes)
* ✔️ Configure interval and break
* ✔️ Task management
* ✔️ Run a pomodoro from task
* ✔️ Save history
* ✔️ Display history and reports
* ✔️ Auto mode, applying intervals and breaks
* ✔️ Choose theme
* ✔️ Tasks in wait and focus mode
* ✔️ Random task from focus list
* ✔️ Localization: EN and PT-BR
* ⏱ Login and register support

## Creation

Created in HTML + Jquery + Spectre.css + BoxIcons. Based on Rarefeitos, a soundboard made by me, following a suggestion by my wife Adrielle.
